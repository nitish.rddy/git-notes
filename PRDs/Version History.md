# Version History

## Objective

* Enable editors and moderators to track how a document has evolved over time
  * Compare between two versions (either branches or same branch at different points of time)
  * See who contributed / edited what part of the document
  * Revert certain part(s) of the document to a particular point in history
  * Enable 'edit' capability on a public scale - ie., anyone should be able to branch and provide their edits. The workflow will make it easy for the owner to accept, reject or ask for revisions in a document.

## Permissions

### MVP

* Only editors and moderators should be able to go into version history mode. Phani: What do you mean by 'version history mode' ? what abilities would it give to the user - revert to older versions? See who contributed what? anything else? Nitish: Yup, that's it. I meant any screen related to user checking out the overall graph or any historical version of the doc / block!
* \#Open-Question Should folks with reply permission also be given access to some parts of version history functionality? Use case being that this person has given a reply and comes back to see something changed in the doc there and wants to see what and whether their input was considered. Phani: One other workflow could be that this user can just ask for edit permission (in a reply) if s/he wants to edit the document? Nitish: Nice idea :)
* Folks with view permission should see only one version which is equal to the master branch of the document

### Beyond MVP

* Permissions at branch level!
  * Use cases:
    1. Team may want to have one public branch, one branch for core team, and maybe some braches for advisors etc.
    2. A user may want to branch out and iterate on a doc with a subset before merginig and making it available to everyone
  * When creating a branch, the person who creates the brach should be able to inherit permissions from master equivalent or a subset of master to iterate together! More details on this needs to go into PRD related to branching and merging. Some base functionality may be needed in MVP too! #ToDo. Phani: I think, in general, we can clean up the permissions flow. I was thinking instead of propagating permisisons directly into the new canvas / branch, we could show a small box on the right rail showing what permissions we are suggesting for this new branch / canvas and give a 'accept' or 'edit / modify' button. The 'edit' button will take them to the right rail (carrying these suggestions forward). I think a similar flow for sub-canvas / canvas creation or while doing drag\&drag in left rail can be followed. Right now, it just feels out of control.

## Ideas for Different Flows

Check out [this Loom Video](https://www.loom.com/share/761021803e0e43a4945498d133aebb80?from\_recorder=1\&recording\_limit=1\&focus\_title=1) for the flow I have in mind and why. Description of the same is below. Phani: agree with the flow Nitish. 2 cents: 1. Just wanted to bring your attention to the feature that we wanted to build that shows a dot or something on a canvas to indicate that something has changed from the last visit. We can see how to integrate this flow into that so that each takes advantage of the other. 2. We need a compare between versions flow for sure. I was thinking if we shoudl include it in MVP. Because in MVP, we can get the basic flow of multi-user collaboration (edit), submit for merge, accept/reject/ review flows. Once that's in place, we can proceed to the next step, maybe. Nitish: 1. Sure, makes sense! 2. Are you suggesting that we can have entire version history outside v1 or only the comparing diffs between any two versions? But yes, I think we need to be smart about which we prioritize among git first so we can get a functional product out quickly! Phani: Let's see what we finalise on tech. This will be an outcome of that. If it's easy, we can do it in V1 iteself.

### View overall history

#### Use case

* User wants to go back a long time and see what changed in the last 3 months
* User wants to get a top-down view of who contributed what. Phani: We need a good design here so that it doesnt' overwhelm the user.

#### Flows

* We can have a button at the top, somewhere near the Share Icon (some icon like a clock or the words 'History' perhaps) that will take the user into a view where we can visualize through a graph how the document (and all tis branches) evolved over time.
* From this view, user can filter out a single bracnh to see its history or can select 1/2 version to compare them

### Block level history

#### Use case

* User is editing a block and feels something changes. He/she wants to see if anyone edited it in the recent past
* User has a change of mind and wants to revert the section to a previous version. Phani: When user reverts to a prev. version, what happens to the edits in this version? Are they deleted? I guess they stay for ever in that branch, unless the commit is deleted? Please correct me. Nitish: I don't think they should be deleted. They can stay on as a previous version. For now, I am visualizing the graph as showing this revert as a new commit that says 'Reverted to version XYZ' or something like that! Need to figure out how we enable this from tech thought

#### Flows

* We can have an option in the six dots menu to go into history mode for a section / block / multiple blocks. Phani: Every history should probably show comparision, instead of displaying the older version in plain. Comparisions can give some meaning to what changed. Nitish: With an option to toggle it off maybe? Sometimes, I just want to read an earlier version by itself fully, especially when its some marketing copy etc. and the comparison only distracts. But this feature can come later too perhaps if we want to keep it simple! Phani: Sure!
* In this view, we can make the rest of the document translucent with a option for user to add more blocks into this view. Phani: We can try on figma to see if translucent is looking better or without translucent is looking better.
* Arrows / Swipe to navigate between consecutive versions in history within the same branch and an option to see its graph (within and across branches) are needed
* The blocks that are highlighted can have some meta data and options to go into a previous or newer version. Phani: Just putting together the meta data: 1. User 2. Timestamp 3. Commit / branch?
* In each version, the default can be a comparison with the immediate previous version with an option to toggle it off
* If I am seeing the history of one block and that block was merged from two blocks in the past, then as we go back, those blocks should also be shown along with history. Similarly for when I am viewing a mutiple blocks and something was added in between. We can't only track by block ID(s) basically and need to track based on context! Phani: let's check if our block ids will persist through the edits, if we use a standard git.
* From this view, user should be able to select two random points in history and compare the versions in this screen itself. Need not navigate into a full canvas 'Compare' mode!

### Compare versions

* User should be able to compare between any two versions either at section level or whole document level.
* User should be easily able to add / remove versions to compare
* testing gitbook integration
