- Things that a collaborator can change
    1. Add / Delete contents of a canvas including replies etc.,
    2. Canvas name
    3. Add sub-canvases
    4. Remove sub-canvases
    5. Change in share settings if one is a moderator
    6. Change location of a canvas in the directory tree?

- git Flows Needed
  - Commit
    - All changes that a user makes in one sitting (session) can be auto-clubbed into one commit equivalent on the branch that user is working on, unless user explicitly commits before end of session.
    - Maybe an option to not commit automatically at the end of session will also help?
    - If the user is not a named editor of that branch(or should it be not a moderator of the canvas? Need to think through), then this commit creates a new merge request into the document that the moderator has to approve. However, in the graph, this need not show as a separate branch that was merged but can show up as an approved commit in the same branch
    - User can choose to add a commit message. This will give a good overview of the changes included in each session in the graph view
    - Edge Case: User is editing a document and lose internet / power. The changes should not auto-commit as user may be midway!

  - Branch
    - A separate branch(es) can be created by the user explicitly when needed 
    - I am thinking that branching and merging can be at canvas level and not studio level. Code needs all files to work toegther. However, thats mostly not the case with documents. There could be this case when a team is working on updating documentation along with a big new feature and multiple canvases need to go hand in hand. But even there, nothing will break by having it separate. Users can just  branch each canvas separately.
    - Branches can have separate permission levels compared to the main branch. This can be used to maintain a separate public / private version or to iterate on a verion with a subset and so on!
    - Only folks with edit permission should be able to branch out??
    - User should be able to connect / disconnect two branches in such a way that changes from one branch reflect in the other and not vice-versa (Use-case - A public version a private version that are always in sync with each other)

  - Merging
    - We can allow named editors to edit directly into that branch without needing a mod approval to make the flow simple? Each commit from a non-named editor can be sent to a mod for accepting / rejecting though in case of public edit. In this case, should the commit create a new branch and then merge back? May look cumbersome in the graph that way. Maybe we can have a way to merge request a commit into a branch in such cases?
    - User should be able to cancel merge reuqests before mod approval / rejects
    - Moderator of the canvas should be able to accept reject a commit / merge request.
    - When merging, like in git, only changes should be merged and not all blocks.
    - Conflicts should be resolved by moderator if they exist
    - When merging, the requesting user should be able to choose whether to delete the source branch upon merging. Mod should be able to ovverride this setting too.
    - Moderator should be able to discuss a merge request with the requster before approving / rejecting or give a reason / message along with approval / rejection.
    - A new canvas created should go for accept / reject from studio and parent canvas mods when published

  - Fork
    - Should we have a fork equivalent? Forking can basically copy an entire canvas / studio into the other user's profile
    - The other user can also choose to keep their canvas updated with a specific branch of the original canvas / studio. They can keep flowing in to this user who has forked a merge requests maybe?

  - Stash and Pop (@paras @giri)
    - Stash is mostly used to keep the changes the user is working on without committing as its not ready yet, but the user has to move on to another branch or pull the latest changes on to this branch
    - In case of documents, stashing may not be needed as committing some changes that are mid  way  wont break anything. 
    - The user can just leave their changes mid-way if the bracnh they are working has acceptable permission levels
    - However, there may be a case where the user is workng directly on an important branch (public facing etc.) and is not ready to commit these changes into that branch. In that case, we need a way for user to commit these changes into a new bracnh created from the branch user is working on. That will allow us to do away with stash and pop in my opinion.
    @Phani: One situation where I see a need for Stash is, if we want to save the sessions as stash and not as commit. We can use commit only for named versions. I am guessing this will help us reduce the overall storage in long term because if we consider each session as a commit, we'll have to persist the changes that are done in that session for ever (or a long term) - we can't delete them because user may want to come back to that version later. In many cases, user may not be interested in going back to sessions. S/he may save the interested versions explicitly by naming a version ('named versions' in google doc parlance) and we can save only these for ever (ie, commit them)

  - Local copy / Staging changes
    - We may not need staging changes as branches can be at each canvas level themselves and each has to be committed separately. We can give flow for user to commit all across canvases if there are pending changes across all maybe?
    - Do we need a local copy in the web app? May be not. But the changes user is actively working on that are saved to the cloud, can still be a private copy of the user until committed!
    - However, the above means that two user's can not work real-time on a single document, see each other's cursors etc. as each time  they edit, its actually in a local copy on the cloud. We can maybe have an option that will enable real time collaboration for the current session so that multiple people can come together on a document.

  - Cherry Pick
    - Maybe not needed as we are thinking of allowing user to see history at block level too and copy pasting from there is easy. And if commits, bracnhes are also at canvas level, then all the more easier, which helps us do away with Cherry Pick.

- What should happen to replies when merging?
  - Should replies also be scoped to a specific branch? Yes, but I am thinking if replies on the primary branch can bubble down to other branches if those blocks exist in the other branch (maybe with a mark on them?) but not vice versa. For example, in the case of 50K use-case, we don't want 50K team to go reply on each branch separately if they want to comment somewhere. 
  - Text based replies can always be scoped to a single branch as they are much more vulnerable to changes (within blocks too basically)
  - They can just comment on the main branch and it will bubble down to all branches. We need to take a call on whether replies to it from people in other branches will also reflect in all branches or only the branches they have access to?
  - When two branches are merged, we can give user option to carry forward replies from the one that is being merged or drop them? Will be useful when a branch is created for internal collaboration and have a lot of replies relevant only to internal team (like discussing word choices etc.) and is merged into a public version!
  - Same flow  for reactions too?

- Notifications
  - Moderators can be notified for each commit?
  - Moderators have to be notified for each merge request
  - Names editors of a branch can also be notified of  each commit and each accepted merge request
  - Clicking a commit notification can land user on the diff

- Design

  1. We need a neat way to show which user added what content, if not in default mode, in some kind of view we need to show this. However, we might want to have this public too because this will encourage users to contribute if it's attributed to them. 
  2. We may want to give a way for a user to do a diff between different versions/ commits. 
  3. Have a graphical UI of which branches are where. https://github.com/git/git-scm.com/network Not sure we need it right away. But writing it down so that we have the long list. 
  4. We have to display commit history (ie., all commit messages and respective meta data - user name, date etc,.) because the commit messages gives a good perspective of how the canvas evolved. 

- Ideas
  1. Maybe create an integration using which exisiting documents in github and gitlab can be ported to bip easily. 
  2. Perhaps we can offer users to backup their documents into their personal / company git by providing an API. Check https://www.xltrail.com/docs/en/stable/git-integration

- Tech
  1. If at all we are going with using git as the underlying version control system directly, Azure DevOps has a hosted solution for git. We can see if it's useful to us and if there's a corresponding thing in AWS
  2. GitLab also has a self-hosted open source solution which we can use. 
  3. When underlying git throws and error we need a way to translate that error to layman terms and offer a way out to the user, besides flagging it for immediate attention. 
  4. When a merge request from earlier commits from the same branch is pending acceptance, I am not able to place a new merge request. Need to understand the reason for this, because for our users this may not matter and we may want to allow the following merge request to flow as smoothly. 

- Resources
  1. Checkout https://www.simuldocs.com/compare/word-online git for MS docs
  2. https://www.xltrail.com/integrations is git for xls 


- Git Vs Subversion 
| Concept | Git | Subversion | Winner
| ------ | ------ | ------ | ------ |
| Save | A save is not necessarily a commit | Looks like we need to commit to save? | Git, if my understanding is right | 
