# Git

## Good video explanation of Git internals by David Mahler
1. https://youtu.be/uR6G2v_WsRA 
2. https://youtu.be/FyAAIHHClqI
3. https://youtu.be/Gg4bLk8cGNo

## A visual git reference, by Mark Lodato:
https://marklodato.github.io/visual-git-guide/index-en.html

# Subversion
Free book: http://svnbook.red-bean.com

# Git like database
Introducing Irmin: Git-like distributed, branchable storage: https://mirage.io/blog/introducing-irmin